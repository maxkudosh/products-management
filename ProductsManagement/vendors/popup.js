﻿define(['jquery'], function ($) {

    $.fn.popup = function (config) {

        config = config || {};

        var defaults = $.fn.popup.defaults;
        var mergedConfig = $.extend({}, defaults, config);

        // Constructing overlay.
        var overlay = getDiv();

        // Default class must be applied.
        overlay.addClass(defaults.overlayClass);
        // Custom class might set background color, change opacity etc.
        if (config.overlayClass) {
            overlay.addClass(config.overlayClass);
        }

        var overlayZIndex = mergedConfig.zIndex;

        overlay.css('z-index', overlayZIndex);

        // Constructing popup itself.

        var popup = getDiv();
        // Default class must be applied
        popup.addClass(defaults.popupClass);
        // Custom class might add some styling.
        if (config.popupClass) {
            popup.addClass(config.popupClass);
        }

        // Popup's z index must be greater then the overlay's z index.
        var popupZIndex = overlayZIndex + 1;
        popup.css('z-index', popupZIndex);

        //var template = $('#' + mergedConfig.templateId).html();
        var template = this.children().detach();
        popup.append(template);

        popup.css({ top: mergedConfig.top, left: mergedConfig.left });
        popup.width(mergedConfig.width);
        popup.height(mergedConfig.height);

        this.append(overlay);
        this.append(popup);

        return this;
    };

    $.fn.popup.defaults = {

        zIndex: 5,

        overlayClass: 'overlay',

        popupClass: 'popup fadeIn',

        markup:'',

        top: '50%',
        left: '50%'

    };

    function constructElement(config) {
        var element;

        if (config.markup) {
            element = config.markup;
        } else {
            element = getDiv();
            element.addClass(config.class);
            element.attr('id', config.id);
            if (config.text) {
                element.html(config.text);
            }
        }

        return element;
    }

    function getDiv() {
        return $('<div>');
    }

    function getButton() {
        return $('<button>');
    }
});