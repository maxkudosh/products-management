﻿define(['backbone', 'js/eventDispatcher', 'js/config'], function (backbone, eventDispatcher, config) {

    var Router = backbone.Router.extend({

        routes: {
            '': 'shop',
            'products': 'products',
            'categories': 'categories',
            'shop': 'shop',
            'cart': 'cart'
        },

        products: function () {
            eventDispatcher.fire(config.routedToProductsEvent);
        },

        categories: function () {
            eventDispatcher.fire(config.routedToCategoriesEvent);
        },

        shop: function () {
            eventDispatcher.fire(config.routedToShopEvent);
        },

        cart: function () {
            eventDispatcher.fire(config.routedToCartEvent);
        }

    });

    function init() {
        var router = new Router();
        backbone.history.start();
    }

    return {
        init: init
    }

});