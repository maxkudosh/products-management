﻿define(['js/utility',
        'js/entities/product',
        'js/dal/localStorage/baseProvider'],
    function (utility, Product, BaseProvider) {

        function ProductProvider() {

            ProductProvider.superclass.constructor.call(this, "product", "productId");

        }

        utility.inherit(ProductProvider, BaseProvider);

        ProductProvider.prototype.toEntity = function (json) {

            return Product.prototype.fromObject(json);

        };

        return ProductProvider;
    });