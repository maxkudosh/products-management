﻿define(['js/utility',
        'js/entities/image',
        'js/dal/localStorage/baseProvider'],
    function (utility, Image, BaseProvider) {

        function ImageProvider() {

            ImageProvider.superclass.constructor.call(this, "image");

        }

        utility.inherit(ImageProvider, BaseProvider);

        ImageProvider.prototype.toEntity = function (json) {

            return Image.prototype.fromObject(json);

        };

        return ImageProvider;
    });