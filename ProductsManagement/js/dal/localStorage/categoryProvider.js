﻿define(['js/utility',
        'js/entities/category',
        'js/dal/localStorage/baseProvider'],
    function (utility, Category, BaseProvider) {

        function CategoryProvider() {

            CategoryProvider.superclass.constructor.call(this, "category");

        }

        utility.inherit(CategoryProvider, BaseProvider);

        CategoryProvider.prototype.toEntity = function (json) {

            return Category.prototype.fromObject(json);

        };

        return CategoryProvider;
    });