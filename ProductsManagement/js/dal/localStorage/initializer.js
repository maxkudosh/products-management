﻿define([], function () {

    var OBJECT_STORES_NAMES = ['category', 'product', 'image'];

    function Initializer() {

    }

    Initializer.prototype.initialize = function () {

        OBJECT_STORES_NAMES.forEach(function (objectStoreName) {

            var objectStore = localStorage.getItem(objectStoreName);

            if (!objectStore) {
                localStorage.setItem(objectStoreName, JSON.stringify([]));
            }

        });

    };

    return Initializer;
});