﻿define([], function () {

    function BaseProvider(type) {

        this.type = type;

    }

    var ID_KEY = "id";

    var getNextId = (function () {

        return function () {

            var lastUsedId = +localStorage.getItem(ID_KEY) || 0;
            var nextId = lastUsedId + 1;
            localStorage.setItem(ID_KEY, nextId);

            return nextId;
        };

    })();

    BaseProvider.prototype.get = function (id) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var localStorageItems = JSON.parse(localStorage.getItem(self.type));

            var localStorageItem = localStorageItems.filter(function (item) {
                return +item.id == id;
            })[0];

            if (localStorageItem) {
                resolve(self.toEntity(localStorageItem));
            } else {
                reject();
            }

        });

        return promise;

    };

    BaseProvider.prototype.getAll = function () {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var localStorageItems = JSON.parse(localStorage.getItem(self.type));

            var items = localStorageItems.map(function (item) {
                return self.toEntity(item);
            });

            resolve(items);

        });

        return promise;
    };

    BaseProvider.prototype.create = function (entity) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var nextId = getNextId();
            entity.id = nextId;

            var localStorageItem = self.toJson(entity);

            var localStorageItems = JSON.parse(localStorage.getItem(self.type));

            localStorageItems.push(localStorageItem);

            localStorage.setItem(self.type, JSON.stringify(localStorageItems));

            resolve(nextId);

        });

        return promise;




    };

    BaseProvider.prototype.update = function (updatedItem) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var localStorageItem = self.toJson(updatedItem);

            var localStorageItems = JSON.parse(localStorage.getItem(self.type));

            var itemToUpdate = localStorageItems.filter(function (item) {
                return item.id == updatedItem.id;
            })[0];
            var index = localStorageItems.indexOf(itemToUpdate);
            localStorageItems[index] = localStorageItem;

            localStorage.setItem(self.type, JSON.stringify(localStorageItems));

            resolve();
        });

        return promise;

    };

    BaseProvider.prototype.delete = function (item) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var localStorageItems = JSON.parse(localStorage.getItem(self.type));

            localStorageItems = localStorageItems.filter(function (localStorageItem) {
                return localStorageItem.id != item.id;
            });

            localStorage.setItem(self.type, JSON.stringify(localStorageItems));

            resolve();
        });

        return promise;

    };

    BaseProvider.prototype.toEntity = function (json) {

        // TO BE OVERRIDEN
        return {};

    };

    BaseProvider.prototype.toJson = function (entity) {

        // This property is added in order to identify entity objects of a given type among other objects in local storage
        entity.type = this.type;
        return entity;

    };

    return BaseProvider;
});