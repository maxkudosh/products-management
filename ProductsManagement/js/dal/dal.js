﻿define([
        'js/dal/indexedDb/initializer',
        'js/dal/indexedDb/productProvider',
        'js/dal/indexedDb/categoryProvider',
        'js/dal/indexedDb/imageProvider'
    ],
    function (Initializer, ProductProvider, CategoryProvider, ImageProvider) {

        var initializer = new Initializer();

        initializer.initialize();

        var dal = {
            product: new ProductProvider(),
            category: new CategoryProvider(),
            image: new ImageProvider()
        };

        return dal;
    });