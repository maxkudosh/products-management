﻿define(['js/config'], function (config) {

    var READ_WRITE_PERMISSIONS = 'readwrite';

    function BaseProvider(type) {

        this.type = type;

    }

    BaseProvider.prototype.get = function (id) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            if (!id) {
                reject();
                return;
            }

            var dbRequest = window.indexedDB.open(config.dbName);

            dbRequest.onsuccess = function (event) {

                var db = event.target.result;

                var transaction = db.transaction([self.type]);

                var store = transaction.objectStore(self.type);

                var getRequest = store.get(id);

                getRequest.onsuccess = function (getResult) {
                    resolve(self.toEntity(getResult.target.result));
                };

            };

            dbRequest.onerror = function () {
                reject();
            };

        });

        return promise;

    };

    BaseProvider.prototype.getAll = function () {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var dbRequest = window.indexedDB.open(config.dbName);

            dbRequest.onsuccess = function (dbOpenedEvent) {

                var db = dbOpenedEvent.target.result;

                var transaction = db.transaction([self.type]);

                var store = transaction.objectStore(self.type);

                var entities = [];

                store.openCursor().onsuccess = function (cursorOpenedEvent) {

                    var cursor = cursorOpenedEvent.target.result;

                    if (cursor) {
                        entities.push(self.toEntity(cursor.value));
                        cursor.continue();
                    } else {
                        resolve(entities);

                    }
                };
            };

            dbRequest.onerror = function () {
                reject();
            };

        });

        return promise;

    };

    BaseProvider.prototype.create = function (entity) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var dbRequest = window.indexedDB.open(config.dbName);

            dbRequest.onsuccess = function (event) {

                var db = event.target.result;

                var transaction = db.transaction([self.type], READ_WRITE_PERMISSIONS);

                // Remove id property, so that the id is autogenerated by DB
                delete entity.id;

                var store = transaction.objectStore(self.type);

                var addRequest = store.add(entity);

                addRequest.onsuccess = function (addResult) {
                    resolve(addResult.target.result);
                };

            };

            dbRequest.onerror = function () {
                reject();
            };

        });

        return promise;

    };

    BaseProvider.prototype.update = function (entity) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var dbRequest = window.indexedDB.open(config.dbName);

            dbRequest.onsuccess = function (event) {

                var db = event.target.result;

                var transaction = db.transaction([self.type], READ_WRITE_PERMISSIONS);

                var store = transaction.objectStore(self.type);

                var updateRequest = store.put(entity);

                updateRequest.onsuccess = function () {
                    resolve();
                };

            };

            dbRequest.onerror = function () {
                reject();
            };
        });



        return promise;

    };

    BaseProvider.prototype.delete = function (entity) {

        var self = this;

        var promise = new Promise(function (resolve, reject) {

            var dbRequest = window.indexedDB.open(config.dbName);

            dbRequest.onsuccess = function (event) {

                var db = event.target.result;

                var transaction = db.transaction([self.type], READ_WRITE_PERMISSIONS);

                var store = transaction.objectStore(self.type);

                var deleteRequest = store.delete(entity.id);

                deleteRequest.onsuccess = function () {
                    resolve();
                };

            };

            dbRequest.onerror = function () {
                reject();
            };

        });

        return promise;

    };

    BaseProvider.prototype.toEntity = function (object) {

        // TO BE OVERRIDEN
        return object;

    };

    //function ()

    return BaseProvider;
});