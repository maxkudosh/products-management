﻿define(['js/utility',
        'js/entities/category',
        'js/dal/indexedDb/baseProvider'],
    function (utility, Category, BaseProvider) {

        function CategoryProvider() {

            CategoryProvider.superclass.constructor.call(this, "category");

        }

        utility.inherit(CategoryProvider, BaseProvider);

        CategoryProvider.prototype.toEntity = function (object) {

            return Category.prototype.fromObject(object);

        };

        return CategoryProvider;
    });