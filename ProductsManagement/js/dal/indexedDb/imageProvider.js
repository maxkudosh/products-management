﻿define(['js/utility',
        'js/entities/image',
        'js/dal/indexedDb/baseProvider'],
    function (utility, Image, BaseProvider) {

        function ImageProvider() {

            ImageProvider.superclass.constructor.call(this, "image");

        }

        utility.inherit(ImageProvider, BaseProvider);

        ImageProvider.prototype.toEntity = function (object) {

            return Image.prototype.fromObject(object);

        };

        return ImageProvider;
    });