﻿define(['js/utility',
        'js/entities/product',
        'js/dal/indexedDb/baseProvider'],
    function (utility, Product, BaseProvider) {

        function ProductProvider() {

            ProductProvider.superclass.constructor.call(this, "product");

        }

        utility.inherit(ProductProvider, BaseProvider);

        ProductProvider.prototype.toEntity = function (object) {

            return Product.prototype.fromObject(object);

        };

        return ProductProvider;
    });