﻿define(['js/config'], function (config) {

    var OBJECT_STORES = ['category', 'product', 'image'];

    function Initializer() {

    }

    Initializer.prototype.initialize = function () {

        var dbRequest = window.indexedDB.open(config.dbName);

        dbRequest.onupgradeneeded = function (event) {

            var db = event.target.result;

            OBJECT_STORES.forEach(function (objectStore) {

                db.createObjectStore(objectStore, { keyPath: 'id', autoIncrement: true });

            });

        };

    };

    return Initializer;
});