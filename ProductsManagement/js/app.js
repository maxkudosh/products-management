﻿require.config({
    baseUrl: '',
    paths: {
        'jquery': 'vendors/jquery',
        'text': 'vendors/require-js-text',
        'popup': 'vendors/popup',
        'ko': 'vendors/knockout',
        'knockout-amd-helpers': 'vendors/knockout-amd-helpers',
        'underscore': 'vendors/underscore',
        'backbone': 'vendors/backbone',
        'bootstrap': 'vendors/bootstrap'
    }
});

require(['js/appStart/vendorsInitialization',
         'js/appStart/viewModelsInitialization',
         'js/appStart/knockoutBindingsInitialization',
         'js/router'],
    function (vendorsInitialization, vmInitializations, knockoutBindings, router) {

        vendorsInitialization.init();
        vmInitializations.init();
        knockoutBindings.init();
        router.init();

    });



