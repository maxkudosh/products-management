﻿define(['js/dal/dal', 'js/utility', 'js/services/baseContextService', 'js/entities/filledProduct'],
    function(dal, utility, BaseContextService, FilledProduct) {

        function ProductService(imageService, categoryService) {

            ProductService.superclass.constructor.call(this, dal.product);

            this.imageService = imageService;
            this.categoryService = categoryService;

        }

        utility.inherit(ProductService, BaseContextService);

        ProductService.prototype.contextFiller = function(product) {

            var categoryPromise = this.categoryService.get(product.categoryId);
            var imagePromise = this.imageService.get(product.imageId);

            return Promise.all([categoryPromise, imagePromise]).then(function (values) {
                return new FilledProduct(product, values[0], values[1]);
            });

        };

        ProductService.prototype.batchContextFiller = function (products) {

            return Promise.all(products.map(this.contextFiller, this));

        };

        return ProductService;

    });