﻿define(function() {

    function BaseService(dataSource) {

        this.dataSource = dataSource;

    }

    BaseService.prototype.getAll = function() {

        return this.dataSource.getAll();

    };

    BaseService.prototype.get = function(id) {

        return this.dataSource.get(id);

    };

    BaseService.prototype.create = function(entity) {

        return this.dataSource.create(entity);

    };

    BaseService.prototype.update = function(entity) {

        return this.dataSource.update(entity);

    };

    BaseService.prototype.delete = function(entity) {

        return this.dataSource.delete(entity);

    };

    return BaseService;
});