﻿define(['js/utility', 'js/services/baseService'],
    function(utility, BaseService) {

        function BaseContextService(dataSource) {

            BaseContextService.superclass.constructor.call(this, dataSource);

        }

        utility.inherit(BaseContextService, BaseService);

        BaseContextService.prototype.contextFiller = function(entity) {
            // to be overriden
            return entity;
        }

        BaseContextService.prototype.batchContextFiller = function (entities) {
            // to be overriden
            return entities;
        }

        BaseContextService.prototype.getAll = function() {

            return this.dataSource.getAll().then(this.batchContextFiller.bind(this));

        };

        BaseContextService.prototype.get = function(id) {

            return this.dataSource.get(id).then(this.contextFiller.bind(this));

        };

        return BaseContextService;
    });