﻿define(['js/services/imageService', 'js/services/categoryService', 'js/services/productService'],
    function(ImageService, CategoryService, ProductService) {

        var services = {};

        services.image = new ImageService();
        services.category = new CategoryService();
        services.product = new ProductService(services.image, services.category);

        return services;
    });