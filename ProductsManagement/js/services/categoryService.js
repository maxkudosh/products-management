﻿define(['js/dal/dal', 'js/utility', 'js/services/baseContextService', 'js/entities/filledCategory'],
    function (dal, utility, BaseContextService, FilledCategory) {

        function CategoryService() {

            CategoryService.superclass.constructor.call(this, dal.category);

        }

        utility.inherit(CategoryService, BaseContextService);

        CategoryService.prototype.contextFiller = function (categoryToFill) {

            if (categoryToFill.parentId) {

                return dal.category.get(categoryToFill.parentId).then(function (parentCategory) {

                    return new FilledCategory(categoryToFill, parentCategory, null);

                });
            } else {

                return dal.category.getAll().then(function(categories) {

                    var children = categories.filter(function(category) {
                        return category.parentId == categoryToFill.id;
                    });

                    return new FilledCategory(categoryToFill, null, children);

                });

            }

        };

        CategoryService.prototype.batchContextFiller = function (categories) {

            return categories.map(function (category) {

                if (category.parentId) {

                    var parentCategory = categories.filter(function (c) {
                        return c.id == category.parentId;
                    })[0];

                    return new FilledCategory(category, parentCategory, null);

                } else {

                    var childrenCategories = categories.filter(function (c) {
                        return c.parentId == category.id;
                    });

                    return new FilledCategory(category, null, childrenCategories);

                }

            });

        };

        CategoryService.prototype.getTopMost = function () {

            return this.getAll().then(function(categories) {
                return categories.filter(function(category) {
                    return !category.parentId;
                });
            });

        };

        CategoryService.prototype.getChildren = function (parentCategoryId) {

            return this.getAll().then(function (categories) {
                return categories.filter(function (category) {
                    return category.parentId && parentCategoryId && category.parentId == parentCategoryId;
                });
            });
        };

        return CategoryService;

    });