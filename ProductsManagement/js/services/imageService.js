﻿define(['js/dal/dal', 'js/utility', 'js/services/baseService', 'js/entities/image', 'js/config'],
    function (dal, utility, BaseService, Image, config) {

        function ImageService() {

            ImageService.superclass.constructor.call(this, dal.image);

        }

        utility.inherit(ImageService, BaseService);

        ImageService.prototype.getDefault = function () {

            return new Promise(function (resolve) {

                var defaultImage = new Image(null, config.defaultImage);

                resolve(defaultImage);

            });

        };

        return ImageService;
    });