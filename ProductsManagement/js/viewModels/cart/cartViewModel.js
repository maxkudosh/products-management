﻿define(['ko',
        'js/config',
        'js/utility',
        'js/cart',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel'],
    function (ko, config, utility, cart, eventDispatcher, BaseViewModel) {

        function CartViewModel() {

            CartViewModel.superclass.constructor.call(this, 'cart', 'cart');

            this.orders = ko.observableArray();

            this.totalCost = ko.pureComputed(function () {

                return this.orders()
                    .map(function (order) {
                        return order.amount() * order.product.cost;
                    })
                    .reduce(function (sum, cost) {
                        return sum + cost;
                    }, 0);

            }, this);

            eventDispatcher.on(config.routedToCartEvent, this.sync, this);

        }

        utility.inherit(CartViewModel, BaseViewModel);

        CartViewModel.prototype.sync = function () {

            var self = this;
            return utility.wrapInPromise(function () {
                self.orders(cart.asArray());
            });

        };

        return CartViewModel;

    });