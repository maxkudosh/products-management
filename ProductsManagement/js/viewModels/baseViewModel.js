﻿define(['ko'], function (ko) {

    function BaseViewModel(container, template) {

        this.container = container;
        this.template = template;

        return this;
    }

    BaseViewModel.prototype.bind = function () {
        var container = document.getElementById(this.container);
        ko.applyBindings(this, container);
    };

    BaseViewModel.prototype.clear = function () {

        for (var property in this) {
            if (ko.isWriteableObservable(this[property])) {
                this[property](null);
            }
        }

    };

    return BaseViewModel;

});