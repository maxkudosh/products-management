﻿define(['ko',
        'js/utility',
        'js/config',
        'js/eventDispatcher',
        'js/services/services',
        'js/viewModels/baseViewModel'
],
    function (ko, utility, config, eventDispatcher, services, BaseViewModel) {

        function FilterViewModel() {

            FilterViewModel.superclass.constructor.call(this, 'filter', 'shop_filter');

            this.topMostCategories = ko.observableArray();

            this.selectedCategory = ko.observable();

            this.subCategories = ko.pureComputed(function () {

                var parentCategory = this.selectedCategory();

                return parentCategory ? parentCategory.children : [];

            }, this);

            this.selectedSubCategory = ko.observable();

        }

        utility.inherit(FilterViewModel, BaseViewModel);

        FilterViewModel.prototype.sync = function () {

            return services.category.getTopMost().then(this.topMostCategories);

        };

        FilterViewModel.prototype.filter = function () {

            var selectedCategory = this.selectedSubCategory() || this.selectedCategory();

            eventDispatcher.fire(config.productsFilteredEvent, selectedCategory);

        };

        return FilterViewModel;

    });