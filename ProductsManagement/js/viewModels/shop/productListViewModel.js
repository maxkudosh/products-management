﻿define([
        'ko',
        'js/utility',
        'js/config',
        'js/cart',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel'
    ],
    function (ko, utility, config, cart, services, eventDispatcher, BaseViewModel) {

        function ProductListViewModel() {

            ProductListViewModel.superclass.constructor.call(this, 'productsList', 'shop_products');

            this.products = [];
            this.categories = [];
            this.filteredProducts = ko.observableArray();

            this.cartCount = cart.count();

            eventDispatcher.on(config.productsFilteredEvent, this.filterProductsByCategory, this);

        }

        utility.inherit(ProductListViewModel, BaseViewModel);

        ProductListViewModel.prototype.sync = function () {

            var self = this;

            var productsPromise = services.product.getAll()
                .then(function (products) {
                    self.products = products;
                    self.filteredProducts(self.products.slice());
                });

            var categoriesPromise = services.category.getAll().then(function (categories) {
                self.categories = categories;
            });

            return Promise.all([productsPromise, categoriesPromise]);

        };

        ProductListViewModel.prototype.filterProductsByCategory = function (selectedCategory) {

            var self = this;

            if (!selectedCategory) {

                self.filteredProducts(self.products.slice());
                return;

            }

            self.filteredProducts.removeAll();

            var filteredProducts = self.products.filter(function (product) {

                return product.category.id == selectedCategory.id
                    || (product.category.parentCategory && product.category.parentCategory.id == selectedCategory.id);
            });

            self.filteredProducts(filteredProducts);

        };

        ProductListViewModel.prototype.addToCart = function (product) {

            cart.add(product);

        };

        return ProductListViewModel;

    });