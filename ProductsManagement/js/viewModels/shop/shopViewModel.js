﻿define(['js/utility',
        'js/config',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel',
        'js/viewModels/shop/filterViewModel',
        'js/viewModels/shop/productListViewModel'],
    function (utility, config, eventDispatcher, BaseViewModel, FilterViewModel, ProductListViewModel) {

        function ShopViewModel() {

            ShopViewModel.superclass.constructor.call(this, "shop", "shop");

            this.filterViewModel = new FilterViewModel();

            this.productListViewModel = new ProductListViewModel();

            eventDispatcher.on(config.routedToShopEvent, this.sync, this);

        }

        utility.inherit(ShopViewModel, BaseViewModel);

        ShopViewModel.prototype.sync = function () {

            return Promise.all([this.productListViewModel.sync(), this.filterViewModel.sync()]);

        };

        ShopViewModel.prototype.bind = function () {

            this.productListViewModel.bind();
            this.filterViewModel.bind();

        };

        return ShopViewModel;

    });