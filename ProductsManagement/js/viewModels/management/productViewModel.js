﻿define(['ko',
        'js/utility',
        'js/config',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel',
        'js/viewModels/management/imageUploadViewModel',
        'js/entities/product'],
    function (ko, utility, config, services, eventDispatcher, BaseViewModel, ImageUploadViewModel, Product) {

        function ProductViewModel() {

            ProductViewModel.superclass.constructor.call(this, 'product', 'product');

            this.topMostCategories = ko.observableArray();

            this.topMostCategoryId = ko.observable();

            this.subCategories = ko.pureComputed(function () {

                var selectedTopMostCategory = utility.filterBy('id', this.topMostCategoryId(), this.topMostCategories());

                return selectedTopMostCategory ? selectedTopMostCategory.children : [];

            }, this);

            this.subCategoryId = ko.observable();

            this.isNewProduct = ko.observable(true);

            this.id = ko.observable();
            this.name = ko.observable();
            this.description = ko.observable();
            this.cost = ko.observable();
            this.amount = ko.observable();

            eventDispatcher.on(config.productEditEvent, function (productToEdit) {

                this.sync().then(this.fill.bind(this, productToEdit));

            }, this);

            eventDispatcher.on(config.productAddEvent, this.sync, this);

            this.imageUploadViewModel = new ImageUploadViewModel();
        }

        utility.inherit(ProductViewModel, BaseViewModel);

        ProductViewModel.prototype.sync = function () {

            var categoriesPromise = services.category.getTopMost().then(this.topMostCategories);
            var imageSyncedPromise = this.imageUploadViewModel.sync();

            return Promise.all([categoriesPromise, imageSyncedPromise]);
        };

        ProductViewModel.prototype.fill = function (product) {

            this.imageUploadViewModel.fill(product.image);

            this.id(product.id);
            this.name(product.name);
            this.description(product.description);
            this.cost(product.cost);
            this.amount(product.totalAmount);

            var productCategoryHasParent = product.category.parentId;

            var topCategoryId = productCategoryHasParent ? product.category.parentId : product.category.id;
            this.topMostCategoryId(topCategoryId);

            var subCategoryId = productCategoryHasParent ? product.category.id : null;
            this.subCategoryId(subCategoryId);

        };

        ProductViewModel.prototype.save = function () {

            var categoryId = this.subCategoryId() ? this.subCategoryId() : this.topMostCategoryId();

            var productToSave = new Product(this.id(),
                                            this.name(),
                                            this.description(),
                                            categoryId,
                                            this.cost(),
                                            this.amount(),
                                            this.imageUploadViewModel.imageId());

            var savePromise = productToSave.id
                ? services.product.update(productToSave) 
                : services.product.create(productToSave).then(this.id);

            var eventToDispatch = productToSave.id ? config.productCreatedEvent : config.productSavedEvent;

            var self = this;
            return savePromise.then(function () {
                    eventDispatcher.fire(eventToDispatch);
                })
                .then(function () {
                    self.clear();
                });
        };

        ProductViewModel.prototype.saveCommand = function () {

            this.imageUploadViewModel.save().then(this.save.bind(this));

        };

        ProductViewModel.prototype.cancelCommand = function () {

            eventDispatcher.fire(config.productCancelEvent);
            this.clear();

        };

        return ProductViewModel;

    });