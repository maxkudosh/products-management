﻿define(['ko',
        'js/utility',
        'js/config',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel'],
    function (ko, utility, config, services, eventDispatcher, BaseViewModel) {

        function ProductsViewModel() {

            ProductsViewModel.superclass.constructor.call(this, 'products', 'products');

            eventDispatcher.on(config.routedToProductsEvent, this.sync, this);
            eventDispatcher.on(config.productSavedEvent, this.sync, this);
            eventDispatcher.on(config.productCreatedEvent, this.sync, this);

            this.products = ko.observableArray();

        }

        utility.inherit(ProductsViewModel, BaseViewModel);

        ProductsViewModel.prototype.sync = function () {
            services.product.getAll().then(this.products);
        };

        ProductsViewModel.prototype.add = function () {
            eventDispatcher.fire(config.productAddEvent);
        };

        ProductsViewModel.prototype.edit = function (product) {
            eventDispatcher.fire(config.productEditEvent, product);
        };

        ProductsViewModel.prototype.delete = function (product) {
            services.product.delete(product).then(this.sync.bind(this));
        };

        return ProductsViewModel;

    });