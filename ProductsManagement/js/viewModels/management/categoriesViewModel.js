﻿define(['ko',
        'js/utility',
        'js/config',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel'],
    function (ko, utility, config, services, eventDispatcher, BaseViewModel) {

        function CategoriesViewModel() {

            CategoriesViewModel.superclass.constructor.call(this, 'categories', 'categories');

            eventDispatcher.on(config.routedToCategoriesEvent, this.sync, this);
            eventDispatcher.on(config.categorySavedEvent, this.sync, this);
            eventDispatcher.on(config.categoryCreatedEvent, this.sync, this);

            this.categories = ko.observableArray();

        }

        utility.inherit(CategoriesViewModel, BaseViewModel);

        CategoriesViewModel.prototype.sync = function () {
            services.category.getAll().then(this.categories);
        };

        CategoriesViewModel.prototype.add = function () {
            eventDispatcher.fire(config.categoryAddEvent);
        };

        CategoriesViewModel.prototype.edit = function (category) {
            eventDispatcher.fire(config.categoryEditEvent, category);
        };

        CategoriesViewModel.prototype.delete = function (category) {
            services.category.delete(category).then(this.sync.bind(this));
        };

        return CategoriesViewModel;

    });