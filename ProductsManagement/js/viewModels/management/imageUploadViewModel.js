﻿define(['ko',
        'js/utility',
        'js/config',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel',
        'js/entities/image'],
    function (ko, utility, config, services, eventDispatcher, BaseViewModel, Image) {

        function ImageUploadViewModel() {

            ImageUploadViewModel.superclass.constructor.call(this, 'image', 'image_upload');

            this.imageId = ko.observable();
            this.imageSource = ko.observable();

        }

        utility.inherit(ImageUploadViewModel, BaseViewModel);

        ImageUploadViewModel.prototype.sync = function () {

            return services.image.getDefault().then(this.fill.bind(this));

        }

        ImageUploadViewModel.prototype.upload = function (file) {

            var self = this;

            if (!file || file.size > config.maxImageSize) {
                return;
            }

            var reader = new FileReader();

            reader.onload = function (e) {

                var bytes = e.target.result;

                self.imageSource(bytes);

            };

            reader.readAsDataURL(file);

        };

        ImageUploadViewModel.prototype.fill = function (image) {

            this.imageId(image.id);
            this.imageSource(image.binaryData);

        };

        ImageUploadViewModel.prototype.save = function () {

            var imageToSave = new Image(this.imageId(), this.imageSource());

            return imageToSave.id
                ? services.image.update(imageToSave)
                : services.image.create(imageToSave).then(this.imageId);

        };

        return ImageUploadViewModel;

    });