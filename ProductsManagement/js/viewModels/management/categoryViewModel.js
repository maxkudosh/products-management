﻿define(['ko',
        'js/utility',
        'js/config',
        'js/services/services',
        'js/eventDispatcher',
        'js/viewModels/baseViewModel',
        'js/entities/category'],
    function (ko, utility, config, services, eventDispatcher, BaseViewModel, Category) {

        function CategoryViewModel() {

            CategoryViewModel.superclass.constructor.call(this, 'category', 'category');

            this.topMostCategories = ko.observableArray();

            this.isNewCategory = ko.observable(true);

            this.id = ko.observable();
            this.name = ko.observable();
            this.description = ko.observable();
            this.parentCategoryId = ko.observable();

            eventDispatcher.on(config.categoryEditEvent, function (categoryToEdit) {

                this.sync().then(this.fill.bind(this, categoryToEdit));

            }, this);

            eventDispatcher.on(config.categoryAddEvent, this.sync, this);

        }

        utility.inherit(CategoryViewModel, BaseViewModel);

        CategoryViewModel.prototype.sync = function () {

            return services.category.getTopMost().then(this.topMostCategories);

        };

        CategoryViewModel.prototype.fill = function (category) {

            this.id(category.id);
            this.name(category.name);
            this.description(category.description);
            this.parentCategoryId(category.parentId);

        };

        CategoryViewModel.prototype.save = function () {

            var categoryToSave = new Category(this.id(),
                                            this.name(),
                                            this.description(),
                                            this.parentCategoryId());

            var savePromise = categoryToSave.id
                ? services.category.update(categoryToSave)
                : services.category.create(categoryToSave).then(this.id);

            var eventToDispatch = categoryToSave.id ? config.categoryCreatedEvent : config.categorySavedEvent;

            var self = this;
            return savePromise
                .then(function () {
                    eventDispatcher.fire(eventToDispatch);
                })
                .then(function () {
                    self.clear();
                });

        };

        CategoryViewModel.prototype.saveCommand = function () {

            this.save();

        };
        CategoryViewModel.prototype.cancelCommand = function () {

            eventDispatcher.fire(config.categoryCancelEvent);
            this.clear();

        };

        return CategoryViewModel;

    });