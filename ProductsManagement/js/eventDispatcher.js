﻿define([], function () {

    var subscriptions = [];

    function on(events, callback, context) {

        if (Array.isArray(events)) {

            events.forEach(function (event) {
                subscriptions.push({ event: event, callback: callback, context: context });
            });

        } else {

            subscriptions.push({ event: events, callback: callback, context: context });

        }

    }

    function off(event, callback) {

        subscriptions = subscriptions.filter(function (subscription) {
            return subscription.event != event && subscription.callback != callback;
        });

    }

    function fire(event, args) {

        var subscriptionsToFire = subscriptions.filter(function (subscription) {
            return subscription.event == event;
        });

        subscriptionsToFire.forEach(function (subscription) {
            subscription.callback.call(subscription.context || window, args);
        });

    }

    return {
        on: on,
        off: off,
        fire: fire
    }

});