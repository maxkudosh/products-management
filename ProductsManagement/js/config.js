﻿define([], function () {

    var config = {

        routedToShopEvent: 'routedToShopEvent',
        routedToProductsEvent: 'routedToProductsEvent',
        routedToCategoriesEvent: 'routedToCategoriesEvent',
        routedToCartEvent: 'routedToCartEvent',

        addToCartEvent: 'addToCart',
        productsFilteredEvent: 'filter',

        categoryCreatedEvent: 'categoryCreatedEvent',
        categorySavedEvent: 'categorySaveEvent',

        categoryAddEvent: 'categoryAddEvent',
        categoryEditEvent: 'categoryEditEvent',
        categoryCancelEvent: 'categoryCancelEvent',

        productCreatedEvent: 'productCreatedEvent',
        productSavedEvent: 'productSaveEvent',

        productAddEvent: 'productAddEvent',
        productEditEvent: 'productEditEvent',
        productCancelEvent: 'productCancelEvent',

        showProductPopup: 'showProductPopup',

        dbName: 'products_management',

        defaultImage: 'img/no-image.png',

        locale: {
            currencySign: '$'
        },

        maxImageSize: 32000

};

    return config;
});