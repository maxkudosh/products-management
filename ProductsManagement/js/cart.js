﻿define(['ko'], function (ko) {

    function Cart() {

        this.orders = {};
        this.length = ko.observable(0);

    }

    Cart.prototype.add = function (product) {

        if (this.orders[product.id]) {

            var productToAdd = this.orders[product.id];

            productToAdd.amount(+productToAdd.amount() + 1);

        } else {

            this.orders[product.id] = {
                product: product,
                amount: ko.observable(1)
            };

            this.length(this.length() + 1);

        }
    };

    Cart.prototype.asArray = function () {
        
        var ordersArray = [];
        
        for (var order in this.orders) {
            ordersArray.push(this.orders[order]);
        }

        return ordersArray;
    };

    Cart.prototype.count = function () {
        return this.length;
    };

    var cart = new Cart();

    return cart;
});