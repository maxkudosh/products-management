﻿define(['js/config'], function (config) {

    function inherit(Child, Parent) {

        var F = function () {
        };

        F.prototype = Parent.prototype;

        Child.prototype = new F();
        Child.prototype.constructor = Child;
        Child.superclass = Parent.prototype;
    }

    function filterBy(field, value, collection) {

        if (!field || !collection) {
            return null;
        }

        return collection.filter(function (item) {
            return item[field] == value;
        })[0];
    }

    function wrapInPromise(f) {

        return new Promise(function (resolve) {
            f();
            resolve();
        });

    }

    function toCurrencyString(string) {
        return string + config.locale.currencySign;
    }

    return {
        inherit: inherit,
        filterBy: filterBy,
        wrapInPromise: wrapInPromise,
        toCurrencyString: toCurrencyString
    };

});