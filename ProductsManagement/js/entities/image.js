﻿define(['js/utility', 'js/entities/baseEntity'], function (utility, BaseEntity) {

    function Image(id, binaryData) {

        Image.superclass.constructor.call(this, id);

        this.binaryData = binaryData;

    }

    utility.inherit(Image, BaseEntity);

    Image.prototype.fromObject = function (object) {
        return new Image(object.id, object.binaryData);
    };

    return Image;

});