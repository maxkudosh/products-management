﻿define(['js/utility', 'js/entities/baseEntity'], function (utility, BaseEntity) {

    function BaseEntityWithName(id, name, description) {

        BaseEntityWithName.superclass.constructor.call(this, id);

        this.name = name;
        this.description = description;

    }

    utility.inherit(BaseEntityWithName, BaseEntity);

    return BaseEntityWithName;

});