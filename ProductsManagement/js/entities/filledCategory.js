﻿define(['js/utility', 'js/entities/category'], function (utility, Category) {

    function FilledCategory(category, parentCategory, children) {

        FilledCategory.superclass.constructor.call(this, category.id, category.name, category.description, parentCategory && parentCategory.id);

        this.parentCategory = parentCategory;
        this.children = children;

    }

    utility.inherit(FilledCategory, Category);

    return FilledCategory;

});