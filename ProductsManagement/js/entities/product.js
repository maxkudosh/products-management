﻿define(['js/utility', 'js/entities/baseEntityWithName'], function (utility, BaseEntityWithName) {

    function Product(id, name, description, categoryId, cost, totalAmount, imageId) {

        Product.superclass.constructor.call(this, id, name, description);

        this.categoryId = categoryId;
        this.cost = cost;
        this.totalAmount = totalAmount;
        this.imageId = imageId;

    }

    utility.inherit(Product, BaseEntityWithName);

    Product.prototype.fromObject = function (object) {
        return new Product(object.id, object.name, object.description, object.categoryId, object.cost, object.totalAmount, object.imageId);
    };

    return Product;

});