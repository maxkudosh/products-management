﻿define(['js/utility', 'js/entities/baseEntityWithName'], function (utility, BaseEntityWithName) {

    function Category(id, name, description, parentId) {

        Category.superclass.constructor.call(this, id, name, description);

        this.parentId = parentId;

    }

    utility.inherit(Category, BaseEntityWithName);

    Category.prototype.fromObject = function (object) {
        return new Category(object.id, object.name, object.description, object.parentId);
    };

    return Category;

});