﻿define(['js/utility', 'js/entities/product'], function (utility, Product) {

    function FilledProduct(product, category, image) {

        FilledProduct.superclass.constructor.call(this, product.id, product.name, product.description, product.categoryId, product.cost, product.totalAmount, product.imageId);

        this.category = category;
        this.categoryName = category.parentCategory ? category.parentCategory.name + "->" + category.name
                                                    : category.name;

        this.image = image;

    }

    utility.inherit(FilledProduct, Product);

    return FilledProduct;

});