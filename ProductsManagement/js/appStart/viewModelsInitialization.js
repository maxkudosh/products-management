﻿define([
        'jquery',
        'popup',
        'ko',
        'knockout-amd-helpers',
        'text',
        'js/config',
        'js/eventDispatcher',
        'js/viewModels/shop/shopViewModel',
        'js/viewModels/management/categoriesViewModel',
        'js/viewModels/management/categoryViewModel',
        'js/viewModels/management/productsViewModel',
        'js/viewModels/management/productViewModel',
        'js/viewModels/cart/cartViewModel'
    ],
    function ($, popup, ko, koHelpers, text, config, eventDispatcher, ShopViewModel, CategoriesViewModel, CategoryViewModel, ProductsViewModel, ProductViewModel, CartViewModel) {

        function init() {

            var shop = new ShopViewModel();
            shop.bind();

            eventDispatcher.on(config.routedToShopEvent, function () {
                hideInactiveViews();
                showActiveView(shop);
            });

            var categoriesManagement = new CategoriesViewModel();
            categoriesManagement.bind();

            eventDispatcher.on(config.routedToCategoriesEvent, function () {
                hideInactiveViews();
                showActiveView(categoriesManagement);
            });

            var categoryManagement = new CategoryViewModel();
            $('#' + categoryManagement.container).popup();
            categoryManagement.bind();

            eventDispatcher.on([
                config.categoryAddEvent,
                config.categoryEditEvent
            ], function () {
                showActiveView(categoryManagement);
                activateTooltips();
            });

            eventDispatcher.on([
                config.categorySavedEvent,
                config.categoryCreatedEvent,
                config.categoryCancelEvent
            ], function () {
                hideActiveView(categoryManagement);
            });

            var productsManagement = new ProductsViewModel();
            productsManagement.bind();

            eventDispatcher.on(config.routedToProductsEvent, function () {
                hideInactiveViews();
                showActiveView(productsManagement);
            });

            var productManagement = new ProductViewModel();
            $('#' + productManagement.container).popup();
            productManagement.bind();

            eventDispatcher.on([
                config.productAddEvent,
                config.productEditEvent
            ], function () {
                showActiveView(productManagement);
                activateTooltips();
            });

            eventDispatcher.on([
                config.productSavedEvent,
                config.productCreatedEvent,
                config.productCancelEvent
            ], function () {
                hideActiveView(productManagement);
            });

            var cart = new CartViewModel();
            cart.bind();

            eventDispatcher.on(config.routedToCartEvent, function () {
                hideInactiveViews();
                showActiveView(cart);
            });

        }

        function hideInactiveViews() {
            $('.view').addClass('hidden');
        }

        function showActiveView(view) {
            $('#' + view.container).removeClass('hidden');
        }

        function hideActiveView(view) {
            $('#' + view.container).addClass('hidden');
        }

        function activateTooltips() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        return {
            init: init
        }
    });