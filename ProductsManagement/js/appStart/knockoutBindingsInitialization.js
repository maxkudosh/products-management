﻿define(['ko'], function (ko) {

    ko.bindingHandlers.attrWithDefault = {

        init: function (element, valueAccessor) {

            var bindings = valueAccessor();

            var value = bindings.value;
            var defaultValue = bindings.default;

            value(defaultValue);

        },

        update: function (element, valueAccessor, allBindingsAccessor, data, context) {

            var bindings = valueAccessor();

            var attrName = bindings.name;
            var value = bindings.value;

            var overridenValueAccessor = function () {

                var newBinding = {};
                newBinding[attrName] = value;

                return newBinding;

            }

            ko.bindingHandlers.attr.update(element, overridenValueAccessor, allBindingsAccessor, data, context);

        }

    };

    function init() {

    }

    return {
        init: init
    }

});